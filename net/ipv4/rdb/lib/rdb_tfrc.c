/*
 * Implementation of Redundant Data Bundling (RDB) in TCP.
 *
 * Copyright (C) 2012-2015, Bendik Rønning Opstad <bro.devel+kernel@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * The TFRC code is based on the TFRC implemention from net/dccp/ccids/
 * and customized for RDB.
 */

#include "rdb_tfrc.h"
#include "loss_history.h"
#include "packet_history.h"

static u32 rdb_tfrc_first_li(struct sock *sk, struct rdb_tfrc *tfrc);

/**
 * rdb_tx_update_s() - Track the mean packet size `s'
 * @tfrc: The rdb_tfrc for the connection
 * @len: Regular packet payload size in bytes
 * @rdb_len: RDB packet payload size in bytes
 *
 * cf. RFC 4342, 5.3 and  RFC 3448, 4.1
 */
inline void rdb_tx_update_s(struct rdb_tfrc *tfrc, int len, int rdb_len)
{
	if (tfrc->tx_stats == NULL) {
		return;
	}
	tfrc->tx_stats->tx_s = tfrc_ewma(tfrc->tx_stats->tx_s, len, 9);
	tfrc->tx_stats->tx_s_rdb = tfrc_ewma(tfrc->tx_stats->tx_s_rdb, rdb_len, 9);
}


/**
 * rdb_tfrc_lh_interval_add() - Handle new loss event(s)
 * @lh:	Loss Interval history
 * @skb: The first SKB in the output queue considered lost
 * @lost_count: The number of lost packets
 * @rtt: Current rtt
 * @calc_first_li: Caller-dependent routine to compute length of first interval
 * @sk:	The socket
 *
 * Note: Based on tfrc_lh_interval_add from dccp/ccids/lib/loss_interval.c
 */
void rdb_tfrc_handle_loss(struct tfrc_loss_hist *lh, struct sk_buff *skb,
			  int lost_count, u32 rtt,
			  u32 (*calc_first_li)(struct sock *, struct rdb_tfrc *),
			  struct sock *sk, struct rdb_tfrc *tfrc)
{
	struct rdb_tfrc_loss_interval *cur = tfrc_lh_peek(lh);

	// First loss event
	if (cur == NULL) {
		rdb_tfrc_interval_add(lh, skb, lost_count, rtt, calc_first_li, sk, tfrc);
		return;
	}

	struct skb_mstamp now;
	skb_mstamp_get(&now);
	u32 diff = skb_mstamp_us_delta(&skb->skb_mstamp, &cur->li_mstamp); // Newest, oldest (Newest - oldest)

	// If longer than one RTT, it's a new loss interval
	if (diff > tfrc->tx_stats->tx_rtt)  {
		rdb_tfrc_interval_add(lh, skb, lost_count, rtt, calc_first_li, sk, tfrc);
	}
	else {
		// Update current inverval with loss count
		rdb_tfrc_lh_update_i_mean(tfrc, lost_count);
	}
}


/**
 * rdb_tfrc_lh_update_i_mean() - Update the loss history
 * @tfrc: The struct containing the tfrc info
 * @lost_count: The number of loss events to add
 *
 * Return: Positive value if the new mean is lower than the old mean, else 0
 */
u8 rdb_tfrc_lh_update_i_mean(struct rdb_tfrc *tfrc, int lost_count)
{
	struct rdb_tfrc_loss_interval *cur = tfrc_lh_peek(tfrc->li_hist);
	u32 old_i_mean = tfrc->li_hist->i_mean;

	if (cur == NULL)
		return 0;

	if (tfrc_lh_length(tfrc->li_hist) == 1)		/* due to RFC 3448, 6.3.1 */
		return 0;

	cur->li_length += lost_count ? lost_count : 1;
	cur->li_lost += lost_count;
	tfrc_lh_calc_i_mean_sp(tfrc->li_hist, tfrc->tx_stats->tx_rtt);
	return tfrc->li_hist->i_mean < old_i_mean;
}


/**
 * rfc3390_initial_rate() - Compute the initial sending rate X_init in the manner of RFC 3390:
 * @tx_stat: The xmit stats for the connection
 *
 *	X_init  =  min(4 * s, max(2 * s, 4380 bytes)) / RTT
 *
 * Note that RFC 3390 uses MSS, RFC 4342 refers to RFC 3390, and rfc3448bis
 * (rev-02) clarifies the use of RFC 3390 with regard to the above formula.
 * For consistency with other parts of the code, X_init is scaled by 2^6.
 *
 * Return: The send rate
 */
static inline u64 rfc3390_initial_rate(struct rdb_tfrc_tx_stats *tx_stats)
{
	const __u32 w_init = clamp_t(__u32, 4380U, 2 * tx_stats->tx_s, 4 * tx_stats->tx_s);
	return scaled_div(w_init << 6, tx_stats->tx_rtt);
}


/**
 * rdb_tfrc_update_loss_history() - Update the loss interval history.
 * @tfrc: The rdb_tfrc struct for the connection.
 * @sk: the socket.
 * @lost_count: The number of packets believed to be lost.
 * @lost: The first SKB believed to be lost.
 */
void rdb_tfrc_update_loss_history(struct rdb_tfrc *tfrc, struct sock *sk,
				  int lost_count, struct sk_buff *lost)
{
	struct rdb_tfrc_tx_stats* tx_stats = tfrc->tx_stats;

	if (lost_count) {
		rdb_tfrc_handle_loss(tfrc->li_hist, lost, lost_count, tx_stats->tx_rtt,
				     rdb_tfrc_first_li, sk, tfrc);
		tfrc_lh_calc_i_mean_sp(tfrc->li_hist, tx_stats->tx_rtt);
	}
	else {
		// Increase interval length
		rdb_tfrc_lh_update_i_mean(tfrc, 0);
	}

	if (tfrc->li_hist->i_mean) {
		tx_stats->tx_p = scaled_div(1, tfrc->li_hist->i_mean);
	}

	/* Update sending rate (step 4 of [RFC 3448, 4.3]) */
	if (tx_stats->tx_p > 0) {
		if (tx_stats->tx_p > 1000000) {
			trace_printk("P too big! tx_stats->tx_p: %u\n", tx_stats->tx_p);
		}
		else { // Calculate the send rate to use
			u32 tx_x_calc = tfrc_calc_x(tx_stats->tx_s,
						    tx_stats->tx_rtt,
						    tx_stats->tx_p);
			// We reduce the send rate by accounting for the network and transport layer headers
			tx_x_calc = (tx_x_calc * tx_stats->tx_s) / (tx_stats->tx_s + 40);
			tx_stats->tx_x_calc = tx_x_calc;
		}
	}
	// Recalculate allowed sending rate X (tx_stats->tx_x, tx_stats->tx_x_cwnd)
	rdb_tx_update_x(tfrc, sk, NULL);
}


/**
 * rdb_tx_update_x() - Update allowed sending rate X
 * @tfrc: The rdb_tfrc struct for the connection
 * @sk: the socket
 * @stamp: most recent time if available (can be left NULL).
 *
 * This function borrows from ccid3_hc_tx_update_x in dccp/ccids/ccid3.c (12/2014)
 */
void rdb_tx_update_x(struct rdb_tfrc *tfrc, struct sock *sk, ktime_t *stamp)
{
	struct rdb_tfrc_tx_stats* tx_stats = tfrc->tx_stats;

	if (!tx_stats->tx_rtt)
		return;

	__u64 min_rate = tx_stats->tx_x >> 1; // Minimum rate is half the current rate

	const __u64 old_x = tx_stats->tx_x;
	ktime_t now = stamp ? *stamp : ktime_get_real();
	u32 delta = ktime_us_delta(now, tx_stats->tx_t_last_sent);
	u32 idle_rtts = delta / tx_stats->tx_rtt;

	/*
	 * Handle IDLE periods: do not reduce below RFC3390 initial sending rate
	 * when idling [RFC 4342, 5.1]. Definition of idling is from rfc3448bis:
	 * a sender is idle if it has not sent anything over a 2-RTT-period.
	 * For consistency with X and X_recv, min_rate is also scaled by 2^6.
	 */
	if (idle_rtts >= 2) {
		min_rate = rfc3390_initial_rate(tx_stats);
	}

	if (tx_stats->tx_p > 0) {
		tx_stats->tx_x = max(((__u64)tx_stats->tx_x_calc) << 6, min_rate);
		trace_printk("rdb_tx_update_x: p: %u, max(tx_x_calc: %llu, min_rate: %llu) -> %llu", tx_stats->tx_p, ((__u64)tx_stats->tx_x_calc) << 6, min_rate, tx_stats->tx_x);

	} else if (ktime_us_delta(now, tx_stats->tx_t_ld) - (s64)tx_stats->tx_rtt >= 0) {
		trace_printk("rdb_tx_update_x ELSE IF: tx_stats->tx_x: %llu, min_rate: %llu\n", tx_stats->tx_x, min_rate);
		//tx_stats->tx_x = min(2 * tx_stats->tx_x, min_rate);
		trace_printk("tx_x = max(tx_x: %llu, tx_s/tx_rtt: %llu)\n", tx_stats->tx_x, scaled_div(((__u64)tx_stats->tx_s) << 6, tx_stats->tx_rtt));
		tx_stats->tx_x = max(tx_stats->tx_x,
				     scaled_div(((__u64)tx_stats->tx_s) << 6, tx_stats->tx_rtt));
		tx_stats->tx_t_ld = now;
	}

	trace_printk(", tx_x: %llu, old_x: %llu", tx_stats->tx_x, old_x);

	if (tx_stats->tx_x != old_x) {
		u64 rtts_per_second = div64_u64(1000000, tx_stats->tx_rtt);
		trace_printk(", rtts_per_second: %llu\n", rtts_per_second);
		// Update the CWND value based on the calculated throughput
		if (rtts_per_second) {
			tx_stats->tx_x_rtt = div64_u64(tx_stats->tx_x >> 6, rtts_per_second);
			tx_stats->tx_x_cwnd	= div64_u64((__u64) tx_stats->tx_x_rtt, tx_stats->tx_s);
			if (tx_stats->tx_x_cwnd == 0) {
				tx_stats->tx_x_cwnd = 1;
			}
		}
	}
	trace_printk(", tx_x_cwnd: %u\n", tx_stats->tx_x_cwnd);
}


/**
 * rdb_tfrc_first_li
 *
 * Determine the length of the first loss interval via inverse lookup.
 * Assume that x_sent can be computed by the throughput equation
 *		    s
 *	x_sent = --------
 *		 R * fval
 * Find some p such that f(p) = fval; return 1/p (scaled).
 */
static u32 rdb_tfrc_first_li(struct sock *sk, struct rdb_tfrc *tfrc)
{
	struct rdb_tfrc_tx_stats* tx_stats = tfrc->tx_stats;
	struct tcp_sock *tp = tcp_sk(sk);
	u32 x_sent, p;
	u64 fval;

	// We base the send rate on the current CWND * MSS
	x_sent = tp->snd_cwnd * tp->rx_opt.mss_clamp;

	if (tx_stats->tx_rtt == 0) {
		tx_stats->tx_rtt = FALLBACK_RTT;
	}

	fval = scaled_div(tx_stats->tx_s, tx_stats->tx_rtt);
	fval = scaled_div32(fval, x_sent);
	p = tfrc_calc_x_reverse_lookup(fval);
	return p == 0 ? ~0U : scaled_div(1, p);
}


int rdb_lib_init()
{
	return rdb_tfrc_li_init();
}


void rdb_lib_exit()
{
	rdb_tfrc_li_exit();
}


/**
 * rdb_tfrc_cong_init() - Initialize the RDB TFRC library for a connection
 * @cong: The RDBCong struct for the connection
 *
 * Return: 0 if success, else 1
 */
int rdb_tfrc_cong_init(struct rdb_tfrc *tfrc)
{
	tfrc->tx_stats = kzalloc(sizeof(struct rdb_tfrc_tx_stats), GFP_ATOMIC);
	if (!tfrc->tx_stats)
		goto out;

	tfrc->tx_stats->tx_x_cwnd = 1;

	tfrc->li_hist = kzalloc(sizeof(struct tfrc_loss_hist), GFP_ATOMIC);
	if (!tfrc->li_hist)
		goto out_free_tx_stats;

	tfrc_lh_init(tfrc->li_hist);
	goto out;
out_free_tx_stats:
	kfree(tfrc->tx_stats);
	return 1;
out:
	return 0;
}

void rdb_tfrc_cong_exit(struct rdb_tfrc *tfrc)
{
	kfree(tfrc->tx_stats);
	tfrc_lh_cleanup(tfrc->li_hist);
	kfree(tfrc->li_hist);
}
