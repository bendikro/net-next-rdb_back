/*
 * Implementation of Redundant Data Bundling (RDB) in TCP.
 *
 * Copyright (C) 2012-2015, Bendik Rønning Opstad <bro.devel+kernel@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * The TFRC code is based on the TFRC implemention from net/dccp/ccids/
 * and customized for RDB.
 */

#include <net/sock.h>
#include "tfrc_equation.h"
#include "rdb_tfrc.h"
#include "loss_history.h"

static struct kmem_cache  *tfrc_lh_slab  __read_mostly;
/* Loss Interval weights from [RFC 3448, 5.4], scaled by 10 */
static const int tfrc_lh_weights[NINTERVAL] = { 10, 10, 10, 10, 8, 6, 4, 2 };

/* implements LIFO semantics on the array */
static inline u8 LIH_INDEX(const u8 ctr)
{
	return LIH_SIZE - 1 - (ctr % LIH_SIZE);
}

/* the `counter' index always points at the next entry to be populated */
inline struct rdb_tfrc_loss_interval *tfrc_lh_peek(struct tfrc_loss_hist *lh)
{
	return lh->counter ? lh->ring[LIH_INDEX(lh->counter - 1)] : NULL;
}

/* given i with 0 <= i <= k, return I_i as per the rfc3448bis notation */
inline u32 tfrc_lh_get_interval_len(struct tfrc_loss_hist *lh, const u8 i)
{
	BUG_ON(i >= lh->counter);
	return lh->counter ? lh->ring[LIH_INDEX(lh->counter - i - 1)]->li_length : 0;
}

/* given i with 0 <= i <= k, return I_i as per the rfc3448bis notation */
inline struct rdb_tfrc_loss_interval *tfrc_lh_get_interval(struct tfrc_loss_hist *lh, const u8 i)
{
	BUG_ON(i >= lh->counter);
	return lh->counter ? lh->ring[LIH_INDEX(lh->counter - i - 1)] : NULL;
}

/*
 *	On-demand allocation and de-allocation of entries
 */
struct rdb_tfrc_loss_interval *tfrc_lh_demand_next(struct tfrc_loss_hist *lh)
{
	if (lh->ring[LIH_INDEX(lh->counter)] == NULL)
		lh->ring[LIH_INDEX(lh->counter)] = kmem_cache_alloc(tfrc_lh_slab,
								    GFP_ATOMIC);
	return lh->ring[LIH_INDEX(lh->counter)];
}

void tfrc_lh_cleanup(struct tfrc_loss_hist *lh)
{
	if (!tfrc_lh_is_initialised(lh))
		return;

	for (lh->counter = 0; lh->counter < LIH_SIZE; lh->counter++) {
		if (lh->ring[LIH_INDEX(lh->counter)] != NULL) {
			kmem_cache_free(tfrc_lh_slab,
					lh->ring[LIH_INDEX(lh->counter)]);
			lh->ring[LIH_INDEX(lh->counter)] = NULL;
		}
	}
}

/**
 * rdb_tfrc_lh_interval_add() - Insert new record into the loss history
 * @lh:	Loss Interval history
 * @skb: The first SKB in the output queue considered lost
 * @lost_count: The number of lost packets
 * @rtt: Current rtt
 * @calc_first_li: Caller-dependent routine to compute length of first interval
 * @sk:	The socket
 *
 * Add a new interval to the loss history.
 *
 * Note: Based on tfrc_lh_interval_add from dccp/ccids/lib/loss_interval.c
 */
int rdb_tfrc_interval_add(struct tfrc_loss_hist *lh, struct sk_buff *skb,
						  int lost_count, u32 rtt,
						  u32 (*calc_first_li)(struct sock *, struct rdb_tfrc *), struct sock *sk,
						  struct rdb_tfrc *tfrc)
{
	struct rdb_tfrc_loss_interval *new, *cur;

	cur = tfrc_lh_peek(lh);

	new = tfrc_lh_demand_next(lh);
	if (unlikely(new == NULL)) {
		return 0;
	}

	new->li_seqno = TCP_SKB_CB(skb)->seq;
	new->li_length = lost_count ? lost_count : 1;
	new->li_tstamp = skb->tstamp;
	new->li_mstamp = skb->skb_mstamp;
	new->li_lost = lost_count;

	// As this will no longer be the first interval, we mark this as short (if it is)
	if (cur) {
		u32 interval_duration = skb_mstamp_us_delta(&new->li_mstamp, &cur->li_mstamp); // Newest, oldest (Newest - oldest)
		if (interval_duration < (2 * rtt))
			cur->is_short = true;
	}

	if (++lh->counter == 1) {
		lh->i_mean = new->li_length = (*calc_first_li)(sk, tfrc);
		return 0;
	}
	return 1;
}

/*
 * This is the tfrc_lh_calc_i_mean from dccp/ccids/lib/loss_interval.c
 */
void tfrc_lh_calc_i_mean(struct tfrc_loss_hist *lh)
{
	u32 i_i, i_tot0 = 0, i_tot1 = 0, w_tot = 0;
	int i, k = tfrc_lh_length(lh) - 1; /* k is as in rfc3448bis, 5.4 */

	if (k <= 0)
		return;

	for (i = 0; i <= k; i++) {
		i_i = tfrc_lh_get_interval_len(lh, i);

		if (i < k) {
			i_tot0 += i_i * tfrc_lh_weights[i];
			w_tot  += tfrc_lh_weights[i];
		}
		if (i > 0)
			i_tot1 += i_i * tfrc_lh_weights[i-1];
	}
	lh->i_mean = max(i_tot0, i_tot1) / w_tot;
}


/**
 * tfrc_lh_calc_i_mean_sp() - Calculate the loss event rate according to TFRC-SP (Small-Packet variant)
 * @lh:	Loss Interval history
 * @rtt: Current rtt
 *
 * Note: Based on tfrc_lh_calc_i_mean from dccp/ccids/lib/loss_interval.c
 */
void tfrc_lh_calc_i_mean_sp(struct tfrc_loss_hist *lh, u32 rtt)
{
	u32 i_i, i_tot0 = 0, i_tot1 = 0, w_tot = 0;
	int i, k = tfrc_lh_length(lh) - 1; /* k is as in rfc3448bis, 5.4 */
	struct rdb_tfrc_loss_interval *tmp, *prev;
	u32 i_duration;
	bool cur_i_short = false;
	struct skb_mstamp now;

	if (k <= 0)
		return;

	skb_mstamp_get(&now);

	// We start at the newest intervals (index 0)
	for (i = 0; i <= k; i++) {
		tmp = tfrc_lh_get_interval(lh, i);
		i_i = tfrc_lh_get_interval_len(lh, i);

		if (i > 0) {
			// It's a short loss interval that spans less than two RTTs
			if (tmp->is_short) {
				i_i = div64_u64(tmp->li_length, tmp->li_lost);
			}
			i_tot1 += i_i * tfrc_lh_weights[i-1];
		}
		else {
			i_duration = skb_mstamp_us_delta(&now, &tmp->li_mstamp); // Newest, oldest (Newest - oldest)
			cur_i_short = i_duration < (rtt * 2);
		}

		if (i < k) {
			i_tot0 += i_i * tfrc_lh_weights[i];
			w_tot += tfrc_lh_weights[i];
		}
		prev = tmp;
	}

	if (cur_i_short) {
		lh->i_mean = i_tot1;
	}
	else {
		lh->i_mean = max(i_tot0, i_tot1);
	}
	/* Handle the extra restriction defined in rfc4828 to only include the
	   latest interval if it's older than 2 * RTT. */
	if (k > 1) {
		struct rdb_tfrc_loss_interval *cur = tfrc_lh_peek(lh);
		ktime_t now = ktime_get_real();
		s64 diff = ktime_us_delta(now, cur->li_tstamp);

		//  If the current loss interval I_0 is "short"
		if (diff < (rtt * 2)) {
			lh->i_mean = i_tot1;
		}
	}
	lh->i_mean /= w_tot;
}


int rdb_tfrc_li_init(void)
{
	tfrc_lh_slab = kmem_cache_create("rdb_tfrc_li_hist",
									 sizeof(struct rdb_tfrc_loss_interval), 0,
									 SLAB_HWCACHE_ALIGN, NULL);
	return tfrc_lh_slab == NULL ? -ENOBUFS : 0;
}

void rdb_tfrc_li_exit(void)
{
	if (tfrc_lh_slab != NULL) {
		kmem_cache_destroy(tfrc_lh_slab);
		tfrc_lh_slab = NULL;
	}
}
