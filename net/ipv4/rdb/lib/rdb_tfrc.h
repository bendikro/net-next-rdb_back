/*
 * Implementation of Redundant Data Bundling (RDB) in TCP.
 *
 * Copyright (C) 2012-2015, Bendik Rønning Opstad <bro.devel+kernel@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * The TFRC code is based on the TFRC implemention from net/dccp/ccids/
 * and customized for RDB.
 */

#ifndef _RDB_TFRC_H
#define _RDB_TFRC_H

#include "tfrc_equation.h"

/*
 * RTT sampling: sanity bounds and fallback RTT value from RFC 4340, section 3.4
 */
#define SANE_RTT_MIN	100
#define FALLBACK_RTT	(USEC_PER_SEC / 5)
#define SANE_RTT_MAX	(3 * USEC_PER_SEC)

/**
 * struct rdb_tfrc_tx_stats - RDB TFRC stats
 * @tx_x:		  Current sending rate in 64 * bytes per second
 * @tx_x_rtt:	  Current sending rate in bytes per RTT
 * @tx_x_cwnd:	  Current sending congestion window in number of packets
 * @tx_x_calc:	  Calculated rate in bytes per second
 * @tx_rtt:		  Estimate of current round trip time in usecs
 * @tx_p:		  Current loss event rate (0-1) scaled by 1000000
 * @tx_s:		  Packet size in bytes
 */
struct rdb_tfrc_tx_stats {
    u64     tx_x;
    u32     tx_x_rtt;
    u32     tx_x_cwnd;
    u32     tx_x_calc;
    u32     tx_rtt;
    u32     tx_p;
    u16     tx_s;
    u16     tx_s_rdb;
    ktime_t tx_first;
    ktime_t tx_t_last_sent;
	ktime_t tx_t_ld;
    u64     tx_bytes_sent;
};


/**
 * struct rdb_tfrc - RDB TFRC stats
 * @tx_stats:		 Xmit stats
 * @li_hist:		 Loss Interval history
 * @tx_hist:		 Packet history
 */
struct rdb_tfrc {
	struct rdb_tfrc_tx_stats *tx_stats;
	struct tfrc_loss_hist *li_hist;
	struct tfrc_tx_hist_entry *tx_hist;
};

#define LOSS_INTERVAL_COUNT 8

/* Parameter t_mbi from [RFC 3448, 4.3]: backoff interval in seconds */
#define TFRC_T_MBI		   64


void rdb_tx_update_s(struct rdb_tfrc *tfrc, int len, int rdb_len);
void rdb_tfrc_handle_loss(struct tfrc_loss_hist *lh, struct sk_buff *skb,
						  int lost_count, u32 rtt,
						  u32 (*calc_first_li)(struct sock *, struct rdb_tfrc *),
						  struct sock *sk, struct rdb_tfrc *tfrc);
void rdb_tx_update_x(struct rdb_tfrc *tfrc, struct sock *sk, ktime_t *stamp);

int rdb_lib_init(void);
void rdb_lib_exit(void);

int rdb_tfrc_cong_init(struct rdb_tfrc *tfrc);
void rdb_tfrc_cong_exit(struct rdb_tfrc *tfrc);

u8 rdb_tfrc_lh_update_i_mean(struct rdb_tfrc *tfrc, int lost_count);
void rdb_tfrc_update_loss_history(struct rdb_tfrc *tfrc, struct sock *sk, int lost_count, struct sk_buff *lost);

#endif	/* _RDB_TFRC_H */
