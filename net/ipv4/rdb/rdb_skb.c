/*
 * Implementation of Redundant Data Bundling (RDB) in TCP.
 *
 * Copyright (C) 2012-2015, Bendik Rønning Opstad <bro.devel+kernel@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <linux/kernel.h>
#include <linux/socket.h>
#include <linux/tcp.h>
#include <linux/slab.h>
#include <linux/ktime.h>
#include <linux/time.h>
#include <linux/skbuff.h>
#include <linux/gfp.h>
#include <net/tcp.h>

#include "rdb_skb.h"
#include "rdb.h"


/**
 * alloc_rdb_skb() - Allocate a new SKB for RDB
 * @sk: the socket
 * @skb: The SKB to copy the header from
 * @data_size: The number of bytes to allocate for the new SKB
 * @gfp_mask: The gfp_t allocation
 *
 * Return: A new SKB containing redundant data, or NULL if memory allocation failed
 */
static struct sk_buff *alloc_rdb_skb(struct sock *sk,
				     struct sk_buff *skb,
				     int data_size,
				     gfp_t gfp_mask)
{
	struct sk_buff *nskb;
	nskb = sk_stream_alloc_skb(sk, data_size, gfp_mask);
	if (!nskb)
		return NULL;
	copy_skb_header(nskb, skb);
	return nskb;
}


/**
 * append_skb_data() - Copies the data from an SKB over to another
 * @from_skb: The SKB to copy data from
 * @to_skb: The SKB to copy data to
 * @from_offset: The data offset of the from_skb. Will be 0 if all the data should be bundled.
 *
 * Return: void
 */
static void append_skb_data(struct sk_buff *from_skb, struct sk_buff *to_skb,
			    u32 from_offset)
{
	/* Copy the linear data and the data from the frags into the linear
	   page buffer on the to_skb.
	*/
	if (skb_copy_bits(from_skb, from_offset,
			  skb_put(to_skb, from_skb->len - from_offset),
			  from_skb->len - from_offset)) {
		BUG();
	}

	TCP_SKB_CB(to_skb)->end_seq = TCP_SKB_CB(from_skb)->end_seq;

	if (from_skb->ip_summed == CHECKSUM_PARTIAL)
		to_skb->ip_summed = CHECKSUM_PARTIAL;

	if (to_skb->ip_summed != CHECKSUM_PARTIAL)
		to_skb->csum = csum_block_add(to_skb->csum, from_skb->csum, to_skb->len);
}


/**
 * create_rdb_skb() - Creates the new RDB SKB and copies all the data into the linear page buffer.
 * @first_skb: The first SKB in the output queue that contains data we will bundle
 * @xmit_skb: This is the SKB that tcp_write_xmit wants to send
 * @sk: the socket
 * @gfp_mask: The gfp_t allocation
 * @byte_count_in_rdb_skb: The total number of data bytes for the new rdb_skb (NEW + Redundant)
 * @first_skb_data_offset: The data offset of the first_skb. Will be 0 if all the data should be bundled.
 *
 * Return: A new SKB containing redundant data, or NULL if memory allocation failed
 */
static struct sk_buff *create_rdb_skb(struct sk_buff *first_skb,
				      struct sk_buff *xmit_skb,
				      struct sock *sk, gfp_t gfp_mask,
				      int byte_count_in_rdb_skb,
				      int first_skb_data_offset)
{
	struct sk_buff *rdb_skb, *tmp_skb;
	u32 from_offset;
	__wsum csum;

	rdb_skb = alloc_rdb_skb(sk, xmit_skb, byte_count_in_rdb_skb, gfp_mask);
	if (!rdb_skb) {
		return 0;
	}

	// Set starting sequence number to the seq of the first + first_skb_data_offset
	TCP_SKB_CB(rdb_skb)->seq = TCP_SKB_CB(first_skb)->seq + first_skb_data_offset;
	RDB_SKB_CB(xmit_skb)->rdb_start_seq = TCP_SKB_CB(rdb_skb)->seq;

	tmp_skb = first_skb;
	from_offset = first_skb_data_offset;

	tcp_for_write_queue_from(tmp_skb, sk) {
		// Copy data from tmp_skb to rdb_skb
		append_skb_data(tmp_skb, rdb_skb, from_offset);
		from_offset = 0;
		// We are at the last skb that should be included (The unsent one)
		if (tmp_skb == xmit_skb)
			break;
	}

	// Create checksum
	// This actually overwrite any csum value set by append_skb_data
	csum = skb_checksum(rdb_skb, 0, rdb_skb->len, 0);
	rdb_skb->csum = csum;
	return rdb_skb;
}


/**
 * rdb_can_bundle_check() - check if redundant data can be bundled
 * @sk: the socket
 * @skb: The SKB with the newest data to be bundled
 * @mss_now: The current mss value
 * @byte_count_in_rdb_skb: Will contain the resulting number of bytes to bundle at exit.
 * @skb_data_offset: The data offset in the first SKB that will be in the bundle
 * @skbs_to_bundle_count: The total number of SKBs to be in the bundle
 *
 * Traverses  the entire write qeueue and checks if there is data to be bundled.
 *
 * Return: The first SKB to be in the bundle, or NULL if no bundling
 */
static struct sk_buff* rdb_can_bundle_check(struct sock *sk,
					    struct sk_buff *xmit_skb,
					    unsigned int mss_now,
					    u32 *byte_count_in_rdb_skb,
					    u32 *skb_data_offset,
					    u32 *skbs_to_bundle_count)
{
	struct sk_buff *first_to_bundle = NULL;
	struct sk_buff *tmp, *skb = xmit_skb->prev;
	u32 skbs_in_bundle_count = 1; // Start on 1 because of the current skb
	u32 offset_in_skb = 0;
	u32 byte_count = xmit_skb->len;
	u32 tp_snd_una = tcp_sk(sk)->snd_una;

	if (!mss_now)
		mss_now = tcp_sk(sk)->mss_cache;

	if (skb_queue_is_first(&sk->sk_write_queue, xmit_skb))
		return NULL;

	// We start at the first skb, and go backwards in the list.
	tcp_for_write_queue_reverse_from_safe(skb, tmp, sk) {

		// This SKB does not contain unacked data, so break off here. This can't really happen?
		if (TCP_SKB_CB(skb)->end_seq < tp_snd_una)
			break;

		// Not enough room to bundle data from this SKB
		if ((byte_count + skb->len) > mss_now) {
			break;
		}

		// Exceeds max_bundle_bytes
		if (sysctl_tcp_rdb_max_bundle_bytes &&
		    ((byte_count + skb->len) > sysctl_tcp_rdb_max_bundle_bytes)) {
			break;
		}

		// Exceeds max_bundle_skbs
		if (sysctl_tcp_rdb_max_bundle_skbs &&
		    (skbs_in_bundle_count > sysctl_tcp_rdb_max_bundle_skbs)) {
			break;
		}

		byte_count += skb->len;
		// Calculate the offset in this SKB. If tp_snd_una is bigger than seq,
		// the first byte in the rdb-bundled SKB will be tp_snd_una.
		offset_in_skb = max(TCP_SKB_CB(skb)->seq, tp_snd_una) - TCP_SKB_CB(skb)->seq;
		skbs_in_bundle_count++;
		first_to_bundle = skb;
	}

	*skb_data_offset = offset_in_skb;
	*byte_count_in_rdb_skb = byte_count;
	*skbs_to_bundle_count = skbs_in_bundle_count;
	return first_to_bundle;
}


/**
 * redundant_data_bundling() - Tries to perform RDB
 * @sk: the socket
 * @xmit_skb: The SKB that should be sent
 * @mss_now: Current MSS
 * @sk: the socket
 * @gfp_mask: The gfp_t allocation
 * @retrans: If this is a retransmit
 *
 * Return: A new SKB containing redundant data, or NULL if no bundling could be performed
 */
struct sk_buff* redundant_data_bundling(struct sk_buff *xmit_skb, unsigned int mss_now,
					struct sock *sk, gfp_t gfp_mask, bool retrans)
{
	u32 byte_count_in_rdb_skb;
	u32 skb_data_offset;
	u32 skb_in_bundle_count;
	struct sk_buff* first_to_bundle, *rdb_skb;

	// This SKB has FIN flag set
	if (TCP_SKB_CB(xmit_skb)->tcp_flags & TCPHDR_FIN) {
		return NULL;
	}

	// SYN flag set, will never happen on send, as SYN packets are not sent through tcp_write_xmit
	if (TCP_SKB_CB(xmit_skb)->tcp_flags & TCPHDR_SYN) {
		return NULL;
	}

	// First in write queue, means no previous SKB to merge with
	if (!retrans && skb_queue_is_first(&sk->sk_write_queue, xmit_skb)) {
		return NULL;
	}

	// Shouldn't previous if-test test for this?
	// Shouldn't prev_skb point to the list-head?
	// TODO: Test this further
	if (xmit_skb->prev == NULL) {
		return NULL;
 	}

	// Previous has been SACKED
	if (TCP_SKB_CB(xmit_skb->prev)->sacked & TCPCB_SACKED_ACKED) {
		return NULL;
	}

	// Find number of (previous) SKBs to get data from
	first_to_bundle = rdb_can_bundle_check(sk, xmit_skb, mss_now, &byte_count_in_rdb_skb,
					       &skb_data_offset, &skb_in_bundle_count);
	if (!first_to_bundle) {
		return NULL;
	}

	// Create an SKB that contains the data from 'skb_in_bundle_count' SKBs.
	rdb_skb = create_rdb_skb(first_to_bundle, xmit_skb, sk, gfp_mask,
				 byte_count_in_rdb_skb, skb_data_offset);
	if (!rdb_skb) {
		return NULL;
	}

	sysctl_tcp_rdb_packet_count++;
	sysctl_tcp_rdb_bytes_bundled += (byte_count_in_rdb_skb - xmit_skb->len);

	return rdb_skb;
}
